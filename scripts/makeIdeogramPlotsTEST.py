#!/usr/bin/env python
import os
import sys
import ROOT
#from ParametricRecoilFunction import *
import optparse
import copy

def rangeTransform(x,a,b):
    """transform to a limited range as Minuit does (cf. https://root.cern.ch/download/minuit.pdf Sec. 1.2.1)"""
    return a+0.5*(b-a)*(ROOT.TMath.Sin(x)+1.0)

def makeMT_lep_recoil( vlep, vrecoil):
    """ calculate the mT based on the lepton pT and recoil """

    assert isinstance( vlep,    ROOT.TVector2 ), "Input lepton pt is not a 2D vector."
    assert isinstance( vrecoil, ROOT.TVector2 ), "Input recoil is not a 2D vector."

    vsum = vlep + vrecoil

    #if DEBUG:
    #   print " lepton pt:",   vlep.Mod(), " lepton phi:",   ROOT.TVector2.Phi_mpi_pi(vlep.Phi())
    #   print " Neutrino pt:", vsum.Mod(), " neutrino phi:", ROOT.TVector2.Phi_mpi_pi(ROOT.TMath.Pi() + vsum.Phi())

    mt2 = 2 * ( vlep.Mod() * ( vlep.Mod() + vsum.Mod() ) + vrecoil.X() * vlep.X() + vrecoil.Y() * vlep.Y() )

    return ROOT.TMath.Sqrt( mt2 )    

def Calculate(vrecoil, vgenW, vlep, key ):
    assert isinstance( vrecoil, ROOT.TVector2 ), "Input recoil is not a 2D vector."
    assert isinstance( vgenW,   ROOT.TVector2 ), "Input W pt is not a 2D vector."
    assert isinstance( vlep,    ROOT.TVector2 ), "Input lepton pt is not a 2D vector."

    if key=='e1':
       return vgenW.Mod() / (vrecoil.Mod() + 1e-5)
    elif key=='e2':
       return ROOT.TVector2.Phi_mpi_pi( vgenW.Phi() + ROOT.TMath.Pi() - vrecoil.Phi() )
    elif key== 'lne1':
       return ROOT.TMath.Log( vgenW.Mod() / (  vrecoil.Mod() + 1e-5 ) )
    elif key=='ux':
       return vrecoil.X()
    elif key=='uy':
       return vrecoil.Y()
    elif key=='scale':
       return vrecoil.Mod()
    elif key=='uparallel':
       return vrecoil.Mod() * ROOT.TMath.Cos( vgenW.Phi() + ROOT.TMath.Pi() - vrecoil.Phi() )
    elif key=='uperp':
       return vrecoil.Mod() * ROOT.TMath.Sin( vgenW.Phi() + ROOT.TMath.Pi() - vrecoil.Phi() )
    elif key=='ulepparallel':
       # parallel to the lepton momentum
       return vrecoil.Mod() * ROOT.TMath.Cos( vlep.Phi() - vrecoil.Phi() )
    elif key=='ulepperp':
       return vrecoil.Mod() * ROOT.TMath.Sin( vlep.Phi() - vrecoil.Phi() )
    elif key=='mT':
       #print vlep, vrecoil, vlep.Mod(), vlep.Phi(), vrecoil.Mod(), vrecoil.Phi(), vlep * vrecoil
       return makeMT_lep_recoil(vlep, vrecoil)
    else:
       print " The key %s does not exist. Please check!!!"%key
       sys.exit(0)

def makeIdeogramPlots(
                      data,
                      pred,
                      maxEvts,
                      #nbins={'scale':200,'direction':100, 'lne1':200, 'e2':100, 'tkmet': 200, 'nVertF': 60, 'parallel': 200, 'perpen': 200},
                      #xmin={'scale': 0.,   'direction':-ROOT.TMath.Pi(), 'lne1':-6.0, 'e2': -ROOT.TMath.Pi(), 'tkmet':    0.,   'nVertF': 0, 'parallel': -100,'perpen': -100},
                      #xmax={'scale': 200., 'direction': ROOT.TMath.Pi(), 'lne1': 6.0, 'e2':  ROOT.TMath.Pi(), 'tkmet':  200.,   'nVertF': 60, 'parallel': 100, 'perpen': 100}
                      nbins = {'scale':160 , 'ux': 120,  'uy': 120 , 'uparallel': 170 , 'uperp': 120 ,  'mT':120, 'nVertF': 60,  'lne1':120 , 'e2':100,               'e1':  100},
                      xmin  = {'scale':-10., 'ux': -60., 'uy': -60., 'uparallel': -20., 'uperp': -60.,  'mT':0.,  'nVertF': 0.,  'lne1':-6.0, 'e2': -ROOT.TMath.Pi(), 'e1': -5.0},
                      xmax  = {'scale':150., 'ux':  60., 'uy':  60., 'uparallel': 150., 'uperp':  60.,  'mT':120, 'nVertF': 60., 'lne1': 6.0, 'e2': ROOT.TMath.Pi(),  'e1':  5.0},
                      xtit  = {'scale': 'W pT [GeV]', 'ux': 'u_{X} [GeV]', 'uy': 'u_{Y} [GeV]', 'uparallel': 'u_{#parallel} [GeV]', 'uperp': 'u_{#perp} [GeV]', 'nVertF': 'Number of Vertices', 'mT': 'W transverse mass [GeV]', 'lne1': 'ln(e1)', 'e2': 'e2', 'e1':'e1'}
                      ):
		      
    # set the binning for diff histograms (used to analyze the bias and resolution in the future)		      
    nbins_diff = copy.deepcopy( nbins )
    xmin_diff  = copy.deepcopy( xmin )
    xmax_diff  = copy.deepcopy( xmax )

    xtit_diff = { val: "(Est.-Truth) of %s"%xtit[val] for val in xtit }

    nbins_diff["scale"] =  100
    xmin_diff ["scale"] = -50.0
    xmax_diff ["scale"] =  50.0

    nbins_diff["uparallel"] =  100
    xmin_diff ["uparallel"] = -50.0
    xmax_diff ["uparallel"] =  50.0

    nbins_diff["mT"] =  100
    xmin_diff ["mT"] = -50.0
    xmax_diff ["mT"] =  50.0

    """Uses the regressed PDF to re-destribute the contribution of a single event"""

    dogPDF=hasattr(pred,'sigma') or hasattr(pred,'qp')

    #prepare histograms
    ideograms={}
    dx,xcen,xof={},{},{}
    xbins = {}
    for c in nbins:
        dx[c]=float((xmax[c]-xmin[c])/nbins[c])
        xcen[c]=[xmin[c]+(xbin+0.5)*dx[c] for xbin in xrange(0,nbins[c])]
        xbins[c]=[xmin[c]+xbin*dx[c] for xbin in xrange(0,nbins[c])]
        xbins[c][0] = 1e-6 if xmin[c]==0 else xbins[c][0]
        xbins[c].append( xmax[c] )
        xof[c] = xcen[c][-1]+0.5*dx[c]


    def fillIdeogram(key,c,val,truth,wgt=1.0):
        """creates an ideogram if not existing and fills it up"""
        name=key+'_'+c
        if not name in ideograms:
           ideograms[name]=ROOT.TH1F('h'+name,'%s;%s;Events'%(name,xtit[c]),nbins[c],xmin[c],xmax[c])
           ideograms[name].Sumw2()
           ideograms[name+'corr']=ROOT.TH2F('hcorr'+name,'%s;True %s; %s;Events'%(name,xtit[c],xtit[c]),nbins[c],xmin[c],xmax[c],nbins[c],xmin[c],xmax[c])
           ideograms[name+'corr'].Sumw2()
        ideograms[name].Fill(val,wgt)
        ideograms[name+'corr'].Fill(truth,val,wgt)

    def fillIdeogramDiff(key,c,reg,truth,xtruth, cx, wgt=1.0):
        """creates an histogram for the difference if not available and fills it up"""
        
        name=key+'_'+c +'_diff_VS_' + cx

        if not name in ideograms:
            ideograms[name]=ROOT.TH2F('h'+name,
                                      ';%s;%s;Events'%(xtit[cx], xtit_diff[c]),
                                      nbins[cx],xmin[cx],xmax[cx],
                                      nbins_diff[c],xmin_diff[c],xmax_diff[c])

            
            ideograms[name].Sumw2()

        if c=='e2' or c == 'direction':
            diff=ROOT.TVector2.Phi_mpi_pi(reg-truth)
        else:
            diff=reg-truth
        ideograms[name].Fill(xtruth,diff,wgt)

    def fillIdeogramResol(key,c, xval, resol, cx, wgt=1.0):
        """creates an histogram for the resolution if not available and fills it up"""
        
        name=key+'_'+c +'_resol_VS_' + cx

        if not name in ideograms:
            if c=='scale':
                yran=(0,100)
            elif c=='direction':
                yran=(0, 2*ROOT.TMath.Pi())
            elif c=='lne1':
                yran=(0, 8)
            elif c=='e2':
                yran=(0, 2*ROOT.TMath.Pi())
            elif c=='parallel':
                yran=(0,100)
            elif c=='perpen':
                yran=(0,100)		

            xtits = {'scale': 'Recoil scale [GeV]', 'direction': 'Recoil direction [rad]', 'lne1': 'lne1', 'e2' : 'e2', 'tkmet': 'Track MET [GeV]', 'nVertF': 'Number of Vertices'}

            ytit = 'Resolution of %s'%(xtits[cx][0].lower()+xtits[cx][1:])

            ideograms[name]=ROOT.TH2F('h'+name,
                                      ';%s;%s;Events'%(xtits[cx], ytit),
                                      nbins[cx]/2,xmin[cx]/2.0,xmax[cx]/2.0,
                                      100,yran[0],yran[1])

            
            ideograms[name].Sumw2()

        ideograms[name].Fill(xval,resol,wgt)

    def fillVarDistributions(key, c, val, truthpt, wgt=1.0):
        """creates an histogram for some varaibles vs genpt if not existing and fills it up"""
        name=key+'_'+c+'_vs_genpt'
        if not name in ideograms:
            ideograms[name]=ROOT.TH2F('h'+name,
                                      ';Recoil scale [GeV];%s;Events'%xtit[c],
                                      nbins['scale'],xmin['scale'],xmax['scale'],
                                      nbins[c], xmin[c], xmax[c])
            ideograms[name].Sumw2()
        ideograms[name].Fill(truthpt,val,wgt)


    #determine which parameterizations have been trained
    pdfParams={}
    data.GetEntry(0)
    semiParamTrain=None
    for c in ['lne1', 'e2', 'e1', 'uxdiff', 'uydiff']:
         if hasattr(pred,'mu_'+c):
             pdfParams[('peak',c)]=['mu_'+c]
             if hasattr(pred,'offset_'+c):
                 semiParamTrain='gd'
                 pdfParams[('gauss',c)]=pdfParams[('peak',c)]+['sigma_'+c]
                 pdfParams[(semiParamTrain,c)]=pdfParams[('gauss',c)]+['aL_'+c,'aR_'+c,'offset_'+c]
             if hasattr(pred,'fL_'+c):
                 semiParamTrain='bgsum'
                 pdfParams[('gauss',c)]=pdfParams[('peak',c)]+['sigma_'+c]
                 pdfParams[(semiParamTrain,c)]=pdfParams[('gauss',c)]+['sigmaL_'+c,'fL_'+c,'sigmaR_'+c,'fR_'+c]
             elif hasattr(pred,'qm_'+c):
                 pdfParams[('gauss',c)]=pdfParams[('peak',c)]+['qm_'+c,'qp_'+c]
    print 'The following PDF parameters are available in this training'
    print pdfParams

    def fillPDFParamValues():

        """wraps up the procedure used to fill the parameters of the PDFs for each event"""

        pdfParamVals={}
        #loop over the required PDFs and fill with the values
        for key in pdfParams:
            pdfParamVals[key]=[]

            for p in pdfParams[key]:
                val=getattr(pred,p)

                #gaussian double expo
                if semiParamTrain=='gd':
                    if key[1]=='lne1':
                        if p=='mu_lne1':        val=rangeTransform(val,-3,3) 
                        elif p=='offset_lne1':  val=0
                        else:                   val=rangeTransform(val,1e-3,5)
                    if key[1]=='e2':
                        if p=='mu_e2':       val=rangeTransform(val,-ROOT.TMath.Pi(),ROOT.TMath.Pi()) 
                        elif p=='offset_e2': val=rangeTransform(val,0,1)
                        elif p=='sigma_e2':  val=rangeTransform(val,1e-3,2*ROOT.TMath.Pi())
                        else:                val=rangeTransform(val,1e-3,ROOT.TMath.Pi())                        

                #bifur gauss+gaussian
                if semiParamTrain=='bgsum':
                    if key[1]=='lne1':
                        if p=='mu_lne1':        val=rangeTransform(val,-3,3) 
                        elif p=='sigma_lne1':   val=rangeTransform(val,1e-3,5) 
                        elif p=='sigmaL_lne1':  val=rangeTransform(val,1e-3,10) 
                        elif p=='fL_lne1':      val=rangeTransform(val,1e-6,1) 
                        elif p=='sigmaR_lne1':  val=rangeTransform(val,1e-3,10)
                        elif p=='fR_lne1':      val=rangeTransform(val,1e-6,1)  
                    if key[1]=='e2':
                        if p=='mu_e2':          val=rangeTransform(val,-0.5*ROOT.TMath.Pi(),0.5*ROOT.TMath.Pi()) 
                        elif p=='sigma_e2':     val=rangeTransform(val,1e-3,ROOT.TMath.Pi()) 
                        elif p=='sigmaL_e2':    val=rangeTransform(val,1e-3,1e4) 
                        elif p=='fL_e2':        val=rangeTransform(val,1e-6,1.0) 
                        elif p=='sigmaR_e2':    val=rangeTransform(val,1e-3,1e4)
                        elif p=='fR_e2':        val=rangeTransform(val,1e-6,1.0)  

                pdfParamVals[key].append( val )
            
            #for the non-parametric quantile-regression transform to a gaussian-like
            #if not semiParamTrain and key[0]=='gauss':
            #    valList=pdfParamVals[key]
            #    if key[1]=='lne1':
            #        sL=abs(valList[2]-valList[0])
            #        sR=abs(valList[0]-valList[1])
            #    else:
            #        sL=abs(valList[2]-valList[0])
            #        sR=abs(valList[0]-valList[1])
            #    pdfParamVals[key]=[valList[0],0.5*(sL+sR)]

        #all done
        return pdfParamVals

    #loop over events
    nentries=data.GetEntries() 
    print "nentries:", nentries
    if maxEvts>0: nentries=min(nentries,maxEvts)
    #for i in xrange(0,nentries):
    idata = 0
    ipred = 0
    while ipred < nentries:

        if not ipred%1000: print "analyzed %d entries"%ipred
        data.GetEntry(idata)
        idata = idata + 1
        if data.isW==0:
           continue

        pred.GetEntry( ipred )
        ipred = ipred + 1

        #print "data: ", data.visgenpt, data.visgenphi, data.isW
        pdfParamVals=fillPDFParamValues()

        debug = 0.0

        # define a few vectors 
        vrecoil_base = ROOT.TVector2()
	vrecoil_base.SetMagPhi( data.htkmet_pt, data.htkmet_phi )

        vgenW = ROOT.TVector2()
	vgenW.SetMagPhi( data.visgenpt, data.visgenphi )

	vrecoil_truth = ROOT.TVector2()
        vrecoil_truth.SetMagPhi( data.visgenpt, data.visgenphi + ROOT.TMath.Pi() )

	vlep = ROOT.TVector2()
	vlep.SetMagPhi( data.GenLepDressed_pt[0], data.GenLepDressed_phi[0] )

        # for plotting the bias and resol of PUPPI MET
        vrecoil_PUPPI = ROOT.TVector2()
        vrecoil_PUPPI.SetMagPhi( data.hpuppimet_pt, data.hpuppimet_phi )

        # regressed result
        for pdf in ['peak']:
            vreg = vrecoil_base
            for ikey, ival in pdfParamVals.iteritems():
                if ikey == (pdf, 'lne1'):
                   vreg = vreg * ROOT.TMath.Exp( ival[0] )
                if ikey == (pdf, 'e2'):
                   vreg = vreg.Rotate( -ival[0] )
                if ikey == (pdf, 'e1'):
                   vreg = vreg * ival[0]
                if ikey == (pdf, 'uxdiff'):
                   vreg = vreg + ROOT.TVector2( ival[0], 0. )
                if ikey == (pdf, 'uydiff'):
                   vreg = vreg + ROOT.TVector2( 0.,  ival[0])

        for c in ['ux', 'uy', 'uparallel', 'uperp', 'mT', 'scale']:
            
	    # truth
	    truth = Calculate( vrecoil_truth, vgenW, vlep, c)

            fillIdeogram('true',c,truth,truth)
            fillVarDistributions('true', c, truth, data.visgenpt)

            # raw
	    #raw = Calculate( 2.075*vrecoil_base, vgenW, vlep, c)
            raw = Calculate( vrecoil_base, vgenW, vlep, c)
         
            fillIdeogram('tk',c,    raw,truth)
            fillIdeogramDiff('tk',c,raw,truth,data.visgenpt, 'scale')
            fillIdeogramDiff('tk',c,raw,truth,data.nVertF,   'nVertF')

            # PUPPI
            puppi = Calculate( vrecoil_PUPPI, vgenW, vlep, c)
             
            fillIdeogram('puppi',c,    puppi, truth)
            fillIdeogramDiff('puppi',c,puppi, truth,data.visgenpt, 'scale')
            fillIdeogramDiff('puppi',c,puppi, truth,data.nVertF, 'nVertF')
            

            reg = Calculate( vreg, vgenW, vlep, c)

            fillIdeogram('peak',c,    reg,truth)
            fillIdeogramDiff('peak',c,reg,truth,data.visgenpt,  'scale'  )
            fillIdeogramDiff('peak',c,reg,truth,data.nVertF,    'nVertF' )

                ##gaussian-based correction
                #if pdf=='gauss':

                #    peakquantile = False
                #    if len(pdfParamVals[key]) == 2:
                #       mu,sigma=pdfParamVals[key]
                #    else:
                #       # peak + quantile training
                #       mu = pdfParamVals[key][0]
                #       sigma = 0.5 * ( pdfParamVals[key][2] - pdfParamVals[key][1] )
                #       if sigma<0. and ( c=='direction' or c=='e2' ):
                #          sigma = sigma + 2 * ROOT.TMath.Pi()
                #       assert sigma>= 0., "Sigma not positive. Please check."
                #       peakquantile = True

                #    resol = None

                #    if c=='scale':
                #        bins   = [ ROOT.TMath.Log(xcen[c][i]/rawVal)                       for i in xrange(0,len(xcen[c])) ]
                #        g_pdf  = [ ROOT.TMath.Gaus(bins[i],mu,sigma,True)*dx[c]/xcen[c][i] for i in xrange(0,len(xcen[c])) ]

                #        if peakquantile:
                #           resol = 0.5 * rawVal * ( ROOT.TMath.Exp( pdfParamVals[key][2] ) - ROOT.TMath.Exp( pdfParamVals[key][1] ) )
                #        #opdf = ROOT.TF1("pgaus", "gaus", xmin[c], xmax[c])
                #        #opdf.SetParameters( 1.0/(2.50662827463100024*sigma), mu, sigma )
                #        #g_pdf2 = [ opdf.Integral( ROOT.TMath.Log(xbins[c][i]/rawVal), ROOT.TMath.Log(xbins[c][i+1]/rawVal)) for i in xrange(0, len(xbins[c])-1) ]
                #        g_norm = sum(g_pdf)
                #        #if g_norm<1.0 : print "integral not 1, ", g_norm
                #        fillIdeogram('gauss',c, xof[c], truth, 1-g_norm)
                #        #for xbin in xrange(0,len(g_pdf)):
                #        #    fillIdeogram('gauss',c, xcen[c][xbin], truth, g_pdf2[xbin])
                #        #    fillIdeogramDiff('gauss',c,xcen[c][xbin],truth,data.visgenpt,g_pdf2[xbin])

                #    if c=='direction':
                #        #bins   = [ ROOT.TVector2.Phi_mpi_pi(xcen[c][i]-rawVal) for i in xrange(0,len(xcen[c])) ]
                #        bins   = [ ROOT.TVector2.Phi_mpi_pi(-xcen[c][i]+rawVal) for i in xrange(0,len(xcen[c])) ]
                #        g_pdf  = [ ROOT.TMath.Gaus(bins[i],mu,sigma,True)*dx[c]      for i in xrange(0,len(xcen[c])) ]
                #        g_norm = sum(g_pdf)
                #        g_pdf  = [ x/g_norm for x in g_pdf ]

                #        if peakquantile:
                #           resol = sigma

                #    elif c=='lne1' or c=='e2':
                #        bins  = xcen[c]
                #        g_pdf  = [ ROOT.TMath.Gaus(bins[i],mu,sigma,True)*dx[c]  for i in xrange(0, len(xcen[c])) ]

                #        if peakquantile:
                #           resol = sigma

                #    for xbin in xrange(0,len(g_pdf)):
                #        fillIdeogram('gauss',c, xcen[c][xbin], truth, g_pdf[xbin])
                #        fillIdeogramDiff('gauss',c,xcen[c][xbin],truth,data.visgenpt, 'scale',   g_pdf[xbin])
                #        fillIdeogramDiff('gauss',c,xcen[c][xbin],truth,data.htkmet_pt,'tkmet',   g_pdf[xbin])
                #        fillIdeogramDiff('gauss',c,xcen[c][xbin],truth,data.lne1,     'lne1',    g_pdf[xbin])
                #        fillIdeogramDiff('gauss',c,xcen[c][xbin],truth,data.nVertF,   'nVertF',  g_pdf[xbin])

                #    if resol:
                #       fillIdeogramResol('gauss', c, data.visgenpt,  resol, 'scale' )
                #       fillIdeogramResol('gauss', c, data.htkmet_pt, resol, 'tkmet' )
                #       fillIdeogramResol('gauss', c, data.lne1,      resol, 'lne1'  )
                #       fillIdeogramResol('gauss', c, data.nVertF,    resol, 'nVertF')

                #    

                ##semi-parametric training
                #if not pdf: continue
                #if pdf!=semiParamTrain: continue

                #valRange=None if c=='lne1' or c == 'scale' else [-ROOT.TMath.Pi(),ROOT.TMath.Pi()]

                ##gaussian double expo-based correction
                #if pdf=='gd':
                #    paramList={'mu':pdfParamVals[key][0],'sigma':pdfParamVals[key][1],'aL':pdfParamVals[key][2],'aR':pdfParamVals[key][3],'offset':pdfParamVals[key][4]}
                #else:
                #    paramList={'mu':pdfParamVals[key][0],'sigma':pdfParamVals[key][1],'sigmaL':pdfParamVals[key][2],'fL':pdfParamVals[key][3],'sigmaR':pdfParamVals[key][4],'fR':pdfParamVals[key][5]}

                #prf=ParametricRecoilFunction(semiParamTrain,paramList,valRange)
                #resol = None

                #if c=='scale':
                #    bins   = [ ROOT.TMath.Log(xcen[c][i]/rawVal) for i in xrange(0,len(xcen[c])) ]
                #    g_pdf  = prf.getEventPDF(bins)
                #    g_pdf  = [ g_pdf[i]*dx[c]/xcen[c][i] for i in xrange(0,len(xcen[c])) ]
                #    g_norm = sum(g_pdf)
                #    if g_norm<1:
                #       fillIdeogram(semiParamTrain, c, xof[c], truth, 1-g_norm)

                #    #randVal=rawVal*ROOT.TMath.Exp(prf.getRandom(-6,6))
                #    #fillIdeogram(semiParamTrain+'random',c, randVal, truth)

                #    median = rawVal*ROOT.TMath.Exp( prf.getQuantile(0.5, -4, 4) )
                #    resol = 0.5*rawVal*( ROOT.TMath.Exp( prf.getQuantile(0.84, -4, 4) ) - ROOT.TMath.Exp( prf.getQuantile(0.16, -4, 4) ) )

                #elif c=='lne1' or c=='e2':
                #    bins = xcen[c]
                #    g_pdf = prf.getEventPDF(bins)
                #    g_pdf = [ g_pdf[i]*dx[c]      for i in xrange(0,len(xcen[c])) ] 

                #    if c=='lne1':
                #       median = prf.getQuantile(0.5, -4, 4)
                #       resol = 0.5 * ( prf.getQuantile(0.84, -4, 4) - prf.getQuantile(0.16, -4, 4) )
                #    else:
                #       median = prf.getQuantile( 0.5, -ROOT.TMath.Pi(),ROOT.TMath.Pi() )
                #       resol = 0.5 * ( prf.getQuantile( 0.84, -ROOT.TMath.Pi(),ROOT.TMath.Pi() ) - prf.getQuantile( 0.16, -ROOT.TMath.Pi(),ROOT.TMath.Pi() ) )
                #       if resol<0.:
                #          resol = resol + 2 * ROOT.TMath.Pi()
                #    
                #if c=='direction':
                #    #bins   = [ ROOT.TVector2.Phi_mpi_pi(xcen[c][i]-rawVal) for i in xrange(0,len(xcen[c])) ]
                #    bins   = [ ROOT.TVector2.Phi_mpi_pi(-xcen[c][i]+rawVal) for i in xrange(0,len(xcen[c])) ]
                #    g_pdf  = prf.getEventPDF(bins)
                #    g_pdf  = [ g_pdf[i]*dx[c]      for i in xrange(0,len(xcen[c])) ]
                #    g_norm = sum(g_pdf)                
                #    g_pdf  = [ x/g_norm for x in g_pdf ]
                #    # why minus rawVal
                #    #randVal=ROOT.TVector2.Phi_mpi_pi(prf.getRandom(-ROOT.TMath.Pi(),ROOT.TMath.Pi())-rawVal)
                #    #fillIdeogram(semiParamTrain+'random',c, randVal, truth)
                #    resol = 0.5 * ( prf.getQuantile( 0.84, -ROOT.TMath.Pi(),ROOT.TMath.Pi() ) - prf.getQuantile( 0.16, -ROOT.TMath.Pi(),ROOT.TMath.Pi() ) )
                #    if resol<0.:
                #       resol = resol + 2 * ROOT.TMath.Pi()

                #for xbin in xrange(0,len(g_pdf)):
                #    fillIdeogram(semiParamTrain,c, xcen[c][xbin], truth, g_pdf[xbin])                        
                #    fillIdeogramDiff(semiParamTrain,c,xcen[c][xbin],truth,data.visgenpt, 'scale',  g_pdf[xbin])
                #    fillIdeogramDiff(semiParamTrain,c,xcen[c][xbin],truth,data.htkmet_pt,'tkmet',  g_pdf[xbin])
                #    fillIdeogramDiff(semiParamTrain,c,xcen[c][xbin],truth,data.lne1,     'lne1',   g_pdf[xbin])
                #    fillIdeogramDiff(semiParamTrain,c,xcen[c][xbin],truth,data.nVertF,   'nVertF', g_pdf[xbin])

                ##fillIdeogram( semiParamTrain+'median', c, median, truth )
                ##fillIdeogramDiff(semiParamTrain+'median', c, median, truth, data.visgenpt,    'scale'  )
                ##fillIdeogramDiff(semiParamTrain+'median', c, median, truth, data.htkmet_pt,   'tkmet'  )
                ##fillIdeogramDiff(semiParamTrain+'median', c, median, truth, data.lne1,        'lne1'   )
                ##fillIdeogramDiff(semiParamTrain+'median', c, median, truth, data.nVertF,      'nVertF' )

                #if resol:
                #   fillIdeogramResol(semiParamTrain, c, data.visgenpt,  resol, 'scale' )
                #   fillIdeogramResol(semiParamTrain, c, data.htkmet_pt, resol, 'tkmet' )
                #   fillIdeogramResol(semiParamTrain, c, data.lne1,      resol, 'lne1'  )
                #   fillIdeogramResol(semiParamTrain, c, data.nVertF,    resol, 'nVertF')
                
    return ideograms


def main():
    """wrapper to be used from command line"""

    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i', '--in' ,          dest='input',       help='Input directory [%default]',            default=None)    
    parser.add_option('-n', '--maxEvts' ,     dest='maxEvts',     help='Max. events to process [%default]',     default=-1, type=int)    
    parser.add_option('-o', '--out' ,         dest='output',      help='output dir [%default]',                 default='plots')
    (opt, args) = parser.parse_args()

    #start the chain of events to plot
    print "start preparing the root files"
    fList=os.path.join(opt.input,'predict/tree_association.txt')
    data=ROOT.TChain('tree')
    #data=ROOT.TChain('data')
    pred=ROOT.TChain('tree')
    with open(fList,'r') as f:
        for x in f.read().split('\n'):
            try:
                dF,tF=x.split()
                data.AddFile(dF)
                pred.AddFile(tF)
            except:
                pass
    #data.AddFriend(tree)

    #make the plots
    ideograms=makeIdeogramPlots(data,pred, opt.maxEvts)

    #save to output
    outdir=os.path.dirname(opt.output)
    if len(outdir)>0: os.system('mkdir -p %s'%outdir)
    fOut=ROOT.TFile.Open('%s'%opt.output,'RECREATE')
    for key in ideograms: 
        ideograms[key].Write()
    fOut.Close()
    

if __name__ == "__main__":
    sys.exit(main())
